package com.qa.automation.utest.testCase;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;

import java.net.MalformedURLException;
import java.net.URL;

public class UiTestCaseBase {
    public WebDriver driver;
    //public RemoteWebDriver driver;

    @BeforeSuite
    public void init() throws MalformedURLException {
        String userDir = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", userDir + "\\chromedriver.exe");
        driver = new ChromeDriver();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");

//        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//        capabilities.setPlatform(Platform.WINDOWS);
//        capabilities.setCapability("chromeOptions", "--no-sandbox");
//        capabilities.setCapability("chromeOptions", "--disable-dev-shm-usage");
//        capabilities.setPlatform(Platform.LINUX);

        //driver = new RemoteWebDriver(new URL("http://192.168.221.129:4444/wd/hub"), options);
    }

    public void browseUrl(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }

    public WebDriver getDriver() {
        return driver;
    }
}
