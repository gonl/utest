package com.qa.automation.utest.testCase.SignUp;

import com.qa.automation.utest.pageObjectsAction.UTestAddAddressPageAction;
import com.qa.automation.utest.pageObjectsAction.UTestSignUpPageAction;
import com.qa.automation.utest.testCase.UiTestCaseBase;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC01_SignUpUTest_Submit extends UiTestCaseBase {
    public UTestSignUpPageAction uTestSignUpPageAction;
    public UTestAddAddressPageAction uTestAddAddressPageAction;
    String FIRST_NAME = "Long";
    String LAST_NAME = "Nguyen";
    String EMAIL = "longnguyen@utest.com";
    int DATE = 24;
    String MONTH = "February";
    int YEAR = 1988;
    private String URL = "https://www.utest.com/signup/personal";

    @BeforeClass
    public void setup() {
        uTestSignUpPageAction = new UTestSignUpPageAction(getDriver());
        uTestAddAddressPageAction = new UTestAddAddressPageAction(getDriver());
    }

    @Test
    public void navigateToUTestPage() {
        System.out.println("Step1: Navigate to Utest Home Page");
        browseUrl(URL);
    }

    @Test(dependsOnMethods = {"navigateToUTestPage"})
    public void inputFirstNameInfo() throws Exception {
        uTestSignUpPageAction.inputFirstName(FIRST_NAME);
    }

    @Test(dependsOnMethods = {"inputFirstNameInfo"})
    public void inputLastNameInfo() throws Exception {
        uTestSignUpPageAction.inputLastName(LAST_NAME);
    }

    @Test(dependsOnMethods = {"inputLastNameInfo"})
    public void inputEmailInfo() throws Exception {
        uTestSignUpPageAction.inputEmail(EMAIL);
    }

    @Test(dependsOnMethods = {"inputEmailInfo"})
    public void selectDateOfBirth() throws Exception {
        uTestSignUpPageAction.selectDateOfBirth(MONTH, DATE, YEAR);
    }

    @Test(dependsOnMethods = {"selectDateOfBirth"})
    public void clickOnNextLocation() throws Exception {
        uTestSignUpPageAction.clickOnNextLocation();
    }

    @Test(dependsOnMethods = {"clickOnNextLocation"})
    public void validateSubmitInfoSuccessfully() throws Exception {
        uTestAddAddressPageAction.validatePageTitleExist();
    }

    @AfterClass
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}
