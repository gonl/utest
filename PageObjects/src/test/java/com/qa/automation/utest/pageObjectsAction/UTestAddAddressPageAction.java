package com.qa.automation.utest.pageObjectsAction;

import com.qa.automation.utest.pageObjects.UTestAddAddressPage;
import org.openqa.selenium.WebDriver;

public class UTestAddAddressPageAction extends UTestAddAddressPage {

    public UTestAddAddressPageAction(WebDriver webDriver) {
        super(webDriver);
    }

    public void validatePageTitleExist() throws Exception {
        validateTitlePresent();
    }
}
