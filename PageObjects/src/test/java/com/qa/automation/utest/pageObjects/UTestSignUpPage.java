package com.qa.automation.utest.pageObjects;

import com.qa.automation.utest.WebTestDriver;
import org.openqa.selenium.WebDriver;

public class UTestSignUpPage {
    WebTestDriver webTestDriver;

    private static final String firstNameTextBox = "//form[@name='userForm']//input[@id='firstName']";
    private static final String lastNameTextBox = "//form[@name='userForm']//input[@id='lastName']";
    private static final String emailTextBox = "//form[@name='userForm']//input[@id='email']";
    private static final String monthDropdownBox = "//form[@name='userForm']//select[@id='birthMonth']";
    private static final String dayDropdownBox = "//form[@name='userForm']//select[@id='birthDay']";
    private static final String yearDropdownBox = "//form[@name='userForm']//select[@id='birthYear']";
    private static final String monthDropdownValue = "//form[@name='userForm']//select[@id='birthMonth']//option[text()='%s']";
    private static final String dayDropdownValue = "//form[@name='userForm']//select[@id='birthDay']//option[text()='%s']";
    private static final String yearDropdownValue = "//form[@name='userForm']//select[@id='birthYear']//option[text()='%s']";
    private static final String nextLocationButton = "//div[contains(@class, 'text-right')]//span[text()='Next: Location']";



    public UTestSignUpPage(WebDriver webDriver) {
        this.webTestDriver = new WebTestDriver(webDriver);
    }

    protected void inputFirstNameValue(String firstName) throws Exception {
        webTestDriver.logger("input first name value");
        webTestDriver.inputText(firstNameTextBox, firstName);
    }

    protected void inputLastNameValue(String lastName) throws Exception {
        webTestDriver.inputText(lastNameTextBox, lastName);
    }

    protected void inputEmailValue(String email) throws Exception {
        webTestDriver.inputText(emailTextBox, email);
    }

    protected void clickOnMonthDropdownBox() throws Exception {
        webTestDriver.clickControl(monthDropdownBox);
    }

    protected void clickOnDayDropdownBox() throws Exception {
        webTestDriver.clickControl(dayDropdownBox);
    }

    protected void clickOnYearDropdownBox() throws Exception {
        webTestDriver.clickControl(yearDropdownBox);
    }

    protected void selectMonthDropDownValue(String monthValue) throws Exception {
        webTestDriver.logger("select month dropdown value");
        String month = String.format(monthDropdownValue, monthValue);
        webTestDriver.clickControl(month);
    }

    protected void selectDayDropDownValue(int dayValue) throws Exception {
        webTestDriver.logger("select day dropdown value");
        String date = String.format(dayDropdownValue, dayValue);
        webTestDriver.clickControl(date);
    }

    protected void selectYearDropDownValue(int yearValue) throws Exception {
        webTestDriver.logger("select year dropdown value");
        String year = String.format(yearDropdownValue, yearValue);
        webTestDriver.inputText(yearDropdownBox, Integer.toString(yearValue));
        webTestDriver.clickControl(year);
    }

    protected void clickNextLocationButton() throws Exception {
        webTestDriver.clickControl(nextLocationButton);
    }
}
