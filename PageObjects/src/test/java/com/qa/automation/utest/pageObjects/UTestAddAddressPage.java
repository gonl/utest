package com.qa.automation.utest.pageObjects;

import com.qa.automation.utest.WebTestDriver;
import org.openqa.selenium.WebDriver;

public class UTestAddAddressPage {
    WebTestDriver webTestDriver;

    private static final String firstNameTextBox = "//h1[@class='step-title']//span[text()='Add your address']";

        public UTestAddAddressPage(WebDriver webDriver) {
            this.webTestDriver = new WebTestDriver(webDriver);
        }

        protected void validateTitlePresent() throws Exception {
            webTestDriver.isElementPresent(firstNameTextBox);
        }
}
