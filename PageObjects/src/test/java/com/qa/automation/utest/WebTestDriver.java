package com.qa.automation.utest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebTestDriver {
    public WebDriver driver;

    public WebTestDriver(WebDriver webDriver) {
        this.driver = webDriver;
    }

    private WebElement getElement(String xPath) throws Exception{
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
//        while(!driver.findElement(By.xpath(xPath)).isDisplayed() && timer < 20) {
//            Thread.sleep(1000);
//            timer += 1;
//        }
        WebElement webElement = driver.findElement(By.xpath(xPath));
        return webElement;
    }

    public void inputText(String xPath, String input) throws Exception {
        WebElement webElement = getElement(xPath);
        webElement.sendKeys(input);
    }

    public void clickControl(String xPath) throws Exception {
        WebElement webElement = getElement(xPath);
        webElement.click();
    }

    public boolean isElementPresent(String xPath) throws Exception {
        boolean isPresent = true;
        if (!getElement(xPath).isDisplayed()) {
            isPresent = false;
        }

        return isPresent;

    }

    public void logger(String logMsg) throws Exception {
        System.out.println(logMsg);
    }
}
