package com.qa.automation.utest.pageObjectsAction;

import com.qa.automation.utest.pageObjects.UTestSignUpPage;
import org.openqa.selenium.WebDriver;

public class UTestSignUpPageAction extends UTestSignUpPage {

    public UTestSignUpPageAction(WebDriver webDriver) {
        super(webDriver);
    }

    public void inputFirstName(String firstName) throws Exception {
        inputFirstNameValue(firstName);
    }

    public void inputLastName(String lastName) throws Exception {
        inputLastNameValue(lastName);
    }

    public void inputEmail(String email) throws Exception {
        inputEmailValue(email);
    }

    public void selectDateOfBirth(String month, int date, int year) throws Exception {
        selectMonth(month);
        selectDay(date);
        selectYear(year);
    }

    public void clickOnNextLocation() throws Exception {
        clickNextLocationButton();
    }

    private void selectMonth(String month) throws Exception {

        clickOnMonthDropdownBox();
        selectMonthDropDownValue(month);
    }

    private void selectDay(int date) throws Exception {
        clickOnDayDropdownBox();
        selectDayDropDownValue(date);
    }

    private void selectYear(int year) throws Exception {
        clickOnYearDropdownBox();
        selectYearDropDownValue(year);
    }

}
